﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    GameObject prefab;
    public GameObject shooter;

    void Start()
    {
        prefab = Resources.Load ("peluru") as GameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject Projectile = Instantiate(prefab) as GameObject;
            Projectile.transform.position = transform.position + shooter.transform.forward;
            Rigidbody rb = Projectile.GetComponent<Rigidbody>();
            rb.velocity = shooter.transform.forward * 30; //kecepatan peluru yang ditembakkan
        }
    }
}
