﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinBox : MonoBehaviour
{
    public static float totalTime, recordTime;
    public string num = "stage1";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            totalTime = (int.Parse(Timer.seconds));
            recordTime += totalTime;
            PlayerPrefs.SetInt("FastTime", (int)recordTime);
            SceneManager.LoadScene(num);

            if (num == "GameOver")
            {
                if (Timer.topTime > recordTime || Timer.topTime == 0)
                {
                    PlayerPrefs.SetInt("YourTopTime", (int) recordTime);
                }
            }

        }
    }
}
