﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void toStartScene(){
        //PlayerPrefs.DeleteKey("Total Score");
        SceneManager.LoadScene("HowToPlay");
    }
    public void toLevel1(){
        SceneManager.LoadScene("stage1");
    }

    public void toLevel2(){
        SceneManager.LoadScene("stage2");
    }

    public void toLevel3(){
        SceneManager.LoadScene("stage3");
    }
}
