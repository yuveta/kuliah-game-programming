﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    GameObject prefab;
    public GameObject shooter;
    public float timer = 2f;
    public Transform shotPos;
    public Transform player;
    public Rigidbody bullet;
    private float shotForce = 1000f;

    void Start()
    {
        prefab = Resources.Load("peluruMusuh") as GameObject;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (Mathf.Round(timer) == 0)
        {
            GameObject Projectile = Instantiate(prefab) as GameObject;
            Projectile.transform.position = transform.position + shooter.transform.forward;
            Rigidbody rb = Projectile.GetComponent<Rigidbody>();
            rb.velocity = shooter.transform.forward * 20; //kecepatan peluru musuh
            timer = 2f;
        }

        transform.LookAt(player.position);
    }

    private void Shot()
    {
        Rigidbody shot = Instantiate(bullet, shotPos.position, shotPos.rotation) as Rigidbody;
        shot.AddForce(shotPos.forward * shotForce);
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "peluru(Clone)") //agar musuh hilang setelah ditembakkan peluru
        {
            Destroy(gameObject);
        }
    }
}
