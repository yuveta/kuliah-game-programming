﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class movePlayer : MonoBehaviour
{
    private float speed;
    private Rigidbody rb; 
    private int lives;
    public Text life;
    private int point;
    public Text pointText;
    private GameObject Player;
    public GameObject PanelResult;
    public GameObject PanelGameOver;
    public Text bestPointText;
    private int currentScore;
    private int totalScore;
    public bool isReachFinish;
    public bool fall;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        speed = 20f;
        lives = 3;
        currentScore = 0;
        isReachFinish = false;
        fall = false;
        Player = GameObject.FindGameObjectWithTag("Player");
        //PlayerPrefs.DeleteKey("Best Poin");
        PlayerPrefs.DeleteKey("Poin");
        //PlayerPrefs.DeleteKey("Total Score");
        PanelResult.gameObject.SetActive(false);
        PanelGameOver.gameObject.SetActive(false);

        if (PlayerPrefs.HasKey("Best Poin") == true)
        {
            bestPointText.text = "HIGHSCORE  : " + PlayerPrefs.GetInt("Best Poin").ToString();
        }
        else
        { 
            bestPointText.text = "HIGHSCORE : 0";
            PlayerPrefs.SetInt("Best Poin", 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        pointText.text = "SCORE : " + currentScore;
        life.text = "LIVES : " + lives;
        PlayerPrefs.SetInt("Lives", lives);

        if(Input.GetKeyDown("space")){
            Vector3 jump = new Vector3(0, 20 ,0);
            rb.AddForce(jump * speed);
        }

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }
    
    void OnTriggerEnter(Collider other){
        if (other.gameObject.name == "Enemy Projectile(Clone)")
        {
            lives -= 1;
            life.text = "LIVES : " + lives;
            PlayerPrefs.SetInt("Lives", lives);
            if (lives == 0)
            {
                enabled = false;
                totalScore += currentScore;
                makeHighscore();
                Destroy(Player);
                //rb.constraints = RigidbodyConstraints.FreezeAll;
                PanelGameOver.gameObject.SetActive(true);
            }
        }

        else if (other.gameObject.name == "Bottom")
        {
            enabled = false;
            totalScore += currentScore;
            makeHighscore();
            Destroy(Player);
            fall = true;
            rb.constraints = RigidbodyConstraints.FreezeAll;
            PanelGameOver.gameObject.SetActive(true);
        }

        else if (other.gameObject.name == "Point")
        {
            Destroy(GameObject.Find("Point"));
           
            currentScore += 1;
            PlayerPrefs.SetInt("Poin", currentScore);
        }

        else if (other.gameObject.name == "Point (1)")
        {
            Destroy(GameObject.Find("Point (1)"));
            
            currentScore += 1;
            PlayerPrefs.SetInt("Poin", currentScore);
        }

        else if (other.gameObject.name == "Point (2)")
        {
            Destroy(GameObject.Find("Point (2)"));
            currentScore += 1;
            PlayerPrefs.SetInt("Poin", currentScore);
        }
        else if (other.gameObject.name == "Finish")
        {
            GameObject.Find("Player");
            enabled = false;
            totalScore = currentScore + PlayerPrefs.GetInt("Total Score");
            
            isReachFinish = true;
            PlayerPrefs.SetInt("Total Score", totalScore);
            makeHighscore();
            //Destroy(Player);
            rb.constraints = RigidbodyConstraints.FreezeAll;
            PanelResult.gameObject.SetActive(true);
        }
        
        //Debug.Log(other.gameObject.name);
        
    }    
    void makeHighscore(){
        if (PlayerPrefs.GetInt("Best Poin") < PlayerPrefs.GetInt("Total Score"))
        {
            PlayerPrefs.SetInt("Best Poin", totalScore);
        }
    }

}
