﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveSphere : MonoBehaviour
{
    private Rigidbody rg;
    public float speed;
    public float moveSpeed;
    public static int lives;
    public Text livesText;
    public GameObject panelGameover;

    // Start is called before the first frame update
    void Start()
    {
        moveSpeed = 8f;
        speed = 5f;
        rg = GetComponent<Rigidbody>();
        lives = 3;

        panelGameover.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space")) {
            Vector3 atas = new Vector3(0, 20, 0);
            rg.AddForce(atas * speed);
        }

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rg.AddForce(movement * speed);

        livesText.text = "Lives :" + lives;

        if (lives == 0)
        {
            panelGameover.SetActive(true);
            enabled = false;
        }

        if (transform.position.y <= -1)
        {
            panelGameover.SetActive(true);
            enabled = false;
        } //untuk masuk ke game over jika bola keluar dari arena
    }
}
