﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text timerText, yourFastestTime, cepat;
    private bool finished;
    public static float startTime, yourRecord, topTime;
    public static string minutes, seconds;

    void Start()
    {
        startTime=Time.time;
    }

    void Update()
    {
        if (finished)
            return;
        float t = Time.time-startTime;

        //string minutes =(Mathf.Round((int)t/60)).ToString();
        seconds=(Mathf.Round(t%60)).ToString("f0");

        timerText.text= "Timer :"+seconds;

        yourRecord = PlayerPrefs.GetInt("FastTime");

        topTime = PlayerPrefs.GetInt("YourTopTime");
        yourFastestTime.text = "Highscore: " + topTime.ToString();

        cepat.text = "Highscore: " + topTime.ToString();

    }
    public void Finish()
    {
        finished=true;
        timerText.color=Color.yellow;
    }
}
