﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Projectile : MonoBehaviour
{
    private movePlayer MovePlayer;
    private float speed;
    private Transform Player;
    private Vector3 Target;
    private Rigidbody rb;
    private Scene scene;

    private int lives;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        MovePlayer = FindObjectOfType<movePlayer>();
        speed = 1f;
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        Target = new Vector3(Player.position.x,0,-Player.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        rb.AddForce(Target * speed);

        lives = PlayerPrefs.GetInt("Lives");
        if (lives == 0 | MovePlayer.isReachFinish == true)
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
        }
    }
}
