﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class ClickTarget : MonoBehaviour{
    private int randNum = 0; //menyimpan nilai acak

    public float timerku = 1.5f; //nilai waktu

    public GameObject[] gridObj;

    private Color warna;

    private int skor;

    public Text skorTeks;

    private int lives;

    public Text livesTeks;

    private bool gameOver;

    public GameObject popup;

    // Start is called before the first frame update
    void Start()
    {
        skor = 0;
        skorTeks.text = "Skor " + skor.ToString();
        lives = 3;
        livesTeks.text = "Lives " + lives.ToString();
        gameOver = false;
        popup.SetActive(false);
    }

    void Update()
    {
        timerku -= Time.deltaTime; //timer menurun
        randNum = UnityEngine.Random.Range(0, 9); //generate nilai acak antara 0-8
        if (Mathf.Round(timerku) == 0 && !gameOver)
            { //jika nilai timerku mencapai 0
                Respawn();
                mati();
        }
        if (gameOver)
        {
            popup.SetActive(true);
        }
    }

    void OnMouseDown()
    {
        skor += 1;
        skorTeks.text = "Skor " + skor.ToString();

        Debug.Log("HIT");
        warna = new Color(Random.value, Random.value, Random.value, 1.0f);
        gameObject.GetComponent<Renderer>().sharedMaterial.color = warna;
        Respawn();
    }
    void Respawn()
    {
        //posisikan object target Cube sesuai posisi salah satu grid
        transform.position = gridObj[randNum].transform.position;
        timerku = 1.5f; //reset nilai timerku
    }

    void mati()
    {
        lives -= 1;
        if (lives == 0)
        {
            gameOver = true;
        }
        livesTeks.text = "lives " + lives.ToString();
    }

    public void Reset()
    {
        SceneManager.LoadScene ("SampleScene");
    }

}
