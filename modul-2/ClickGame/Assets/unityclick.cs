﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;


public class unityclick : MonoBehaviour
{
    private int randNum = 0;
    public float timerku = 1.5f;
    public GameObject[] gridObj;
    private int skor;
    public Text skorTeks;
    private int lives;
    public Text livesTeks;
    private bool gameOver;
    public GameObject popup;
    public Text TeksHigh;
    private int high;

    // Start is called before the first frame update
    void Start()
    {
        skor = 0;
        
        lives = 3;
        popup.SetActive(false);
        high = PlayerPrefs.GetInt("highscore");
    }

    // Update is called once per frame
    void Update()
    {
        skorTeks.text = "Skor " + skor.ToString();
        livesTeks.text = "Lives " + lives.ToString();
        TeksHigh.text = "Highscore " + high;
        if (lives == 0)
        {
            if (skor > PlayerPrefs.GetInt("highscore")){
                high = skor; 
                PlayerPrefs.SetInt("highscore", high); }
            Debug.Log("Game Over");
            popup.SetActive(true);

        }else
        {
            timerku -= Time.deltaTime; //timer menurun
            randNum = UnityEngine.Random.Range(0, 3); //generate nilai acak antara 0-8
            Debug.Log("HIT");
            if (Mathf.Round(timerku) == 0)
            {
                Respawn();
                lives -= 1;
            }
        }
        

    }

    void OnMouseDown()
    {
        skor += 1;
        skorTeks.text = "Skor " + skor.ToString();

        Debug.Log("HIT");

        Respawn();
    }

    void Respawn()
    {
        transform.position = gridObj[randNum].transform.position;
        timerku = 1.5f;
    }

    public void Reset()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
