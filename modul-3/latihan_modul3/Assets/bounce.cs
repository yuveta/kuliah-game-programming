﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bounce : MonoBehaviour {
	private Rigidbody rg;
	public float speed,jumpSpeed;


	// Use this for initialization
	void Start () {
		rg=GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (2)) {
			Vector3 atas=new Vector3(0f,20f,0f);
			rg.AddForce (atas * jumpSpeed);
		}
		if (Input.GetMouseButtonDown (0)) {
			Vector3 kanan=new Vector3(20f,0f,0f);
			rg.AddForce (kanan * speed);
		}
		if (Input.GetMouseButtonDown (1)) {
			Vector3 kiri=new Vector3(-20f,0f,0f);
			rg.AddForce (kiri * speed);
		}
	}
}
