﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mousescript : MonoBehaviour {
	Vector3 minScale = new Vector3(0.1f,0.1f,0.1f);
	Vector3 maxScale = new Vector3(3,3,3);
	float distance =10,moveSpeed=1;
	public GameObject[] shape;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		/*if (Input.GetMouseButton (0)) {
			transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 10f));
			Debug.Log (Input.mousePosition);
		}
		if (Input.GetMouseButton (0)) {
			transform.position -= new Vector3 (0.1f, 0, 0);
		}*/
		if (Input.GetMouseButton (1)) {
			transform.position += new Vector3 (0.1f, 0, 0);
		}
		if (Input.GetMouseButton (2)) {
			transform.position += new Vector3 (0,0.1f, 0);
		}

		float zoomValue = Input.GetAxis ("Mouse ScrollWheel");
		if (zoomValue != 0) {
			transform.localScale += Vector3.one * zoomValue;
			transform.localScale = Vector3.Max (transform.localScale,minScale);
			transform.localScale = Vector3.Min (transform.localScale,maxScale);
		}

		if (Input.GetKey ("w")) {
			//transform.position.y += moveSpeed * Time.deltaTime;
			transform.position += new Vector3 (0,0.1f, 0);
		}
		if (Input.GetKey ("s")) {
			//transform.position.y -= moveSpeed * Time.deltaTime;
			transform.position -= new Vector3 (0,0.1f, 0);
		}
		if (Input.GetKey ("d")) {
			//transform.position.x += moveSpeed * Time.deltaTime;
			transform.position += new Vector3 (0.1f, 0, 0);

		}
		if (Input.GetKey ("a")) {
			//transform.position.x -= moveSpeed * Time.deltaTime;
			transform.position -= new Vector3 (0.1f, 0, 0);
		}


		if (Input.GetKey ("z")) {
			GetComponent<MeshFilter>().mesh=shape[0].GetComponent<MeshFilter>().mesh;
		}
		if (Input.GetKey ("x")) {
			GetComponent<MeshFilter>().mesh=shape[1].GetComponent<MeshFilter>().mesh;
		}
		if (Input.GetKey ("c")) {
			GetComponent<MeshFilter>().mesh=shape[2].GetComponent<MeshFilter>().mesh;
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			Debug.Log ("Spasi");
			transform.localScale +=new Vector3(1f,1f,1f);
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			awal ();
		}


	}
	void OnMouseDrag(){
		Vector3 mousePosition = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, distance);
		Vector3 objPosition = Camera.main.ScreenToWorldPoint (mousePosition);
		transform.position = objPosition;

	}


	void awal(){
		transform.position=new Vector3 (0, 0, 0);
		transform.localScale =new Vector3(1f,1f,1f);
	}
}
